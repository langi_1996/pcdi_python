'''
Created on 05.11.2023

@author: langwch
'''

from pcdi.I_PhysicalLayer import I_PhysicalLayer
import copy

from util.AperiodicThread import AperiodicThread

import asyncio
from bleak import BleakScanner, BleakClient, BleakGATTCharacteristic

class BLE_Scanner():
    def __init__(self):
        self.is_scanning = False
        self.scan_thread = None
        self.discovered_devices = []
        self.listener = []

    def addListener(self, l):
        if(l in self.listener):
            return 
        self.listener.append(l)

    def removeListener(self, l):
        if(l in self.listener):
            self.listener.remove(l)

    def scan_for_devices(self):
        if(self.is_scanning):
            return
        
        if(self.scan_thread is None):
            self.scan_thread = AperiodicThread(execute=self.start)
            self.scan_thread.start()
        
            #print("scanning started")
    
    def get_discovered_devices(self):
        return self.discovered_devices
    
    def start(self):
        self.is_scanning = True
        self.stop_event = asyncio.Event()
        loop = asyncio.new_event_loop()
        loop.create_task(self.scan_loop())
        self.fut = loop.create_future()
        loop.run_until_complete(self.fut)   
        
    def stop(self):
        self.stop_event.set()
        self.scan_thread = None
        
        
    def callback(self, device, advertising_data):
        if(device.name is not None):
            for d in self.discovered_devices:
                if(d.address == device.address): 
                    return 
            self.discovered_devices.append(device)
            for l in self.listener:
                l.notifyDeviceDiscovered(device)

    
    async def scan_loop(self):
        #print("entered scan loop")
        async with BleakScanner(self.callback) as scanner:
            await self.stop_event.wait()
        self.is_scanning = False
        #print("left scan loop")
        


class PCDI_BLE_Adapter(I_PhysicalLayer):

    def __init__(self, ble_device_address):
        
        self.ble_device_address = ble_device_address
        self.is_open = False
        self.rx_buffer = []
        self.tx_buffer = []
        self.thread = None
        
    def open(self):

        if(self.is_open):
            return
        
        self.is_open = True
        
        if(self.thread is None):
            self.thread = AperiodicThread(execute=self.start)
            self.thread.start()
        
        print("done")
    
    def start(self):
        loop = asyncio.new_event_loop()
        loop.create_task(self.cyclic())
        self.fut = loop.create_future()
        loop.run_until_complete(self.fut)   
    
    def close(self):
        self.is_open = False
        
        if(self.thread is not None):
            self.thread = None

        
    
    def isOpen(self):
        return self.is_open

    def switchBaudrate(self, baudrate):
        pass
    
    def configureForPCDIMode(self):
        pass
    
    def configureForBootloaderMode(self):
        pass
    
    def resetCommunicationBuffer(self):
        self.rx_buffer = []
    
    def writeAll(self, data):
        for d in data:
            self.tx_buffer.append(d)
        #print(type(data))
        #self.s.sendall(data)
        pass
    
    def callback(self, sender: BleakGATTCharacteristic, data: bytearray):
        #print(f"{sender}: {data}")
        for d in data:
            #b =  int.from_bytes(d, "big")
            self.rx_buffer.append(d)
        
        #print(self.rx_buffer)
    async def cyclic(self):
        
        #devs = await BleakScanner.discover(return_adv=True)
        #print(type(devs))
        #print(devs)
    
        #for key in devs:
        #    print(key, "->", devs[key])
        
        async with BleakClient(self.ble_device_address) as client:
            await client.start_notify("0783b03e-8535-b5a0-7140-a304d2495cb8", self.callback)
            while self.is_open:
                if(len(self.tx_buffer) > 0):
                    #print(self.tx_buffer)
                    await client.write_gatt_char("0783b03e-8535-b5a0-7140-a304d2495cba", bytearray(self.tx_buffer), response=False)
                    self.tx_buffer.clear()
                await asyncio.sleep(0.001)
        
        self.fut.set_result(True)
        #try:
        #    b = int.from_bytes(self.s.recv(1), "big")
        #    self.rx_buffer.append(b)
        #except Exception:
        #    pass
    
    def readAll(self):     
        #print(self.rx_buffer)   
        res = copy.deepcopy(self.rx_buffer)
        self.rx_buffer = []
        return res

    def numberOfBytesInWaiting(self):
        #print(self.rx_buffer)
        return len(self.rx_buffer)


    
    