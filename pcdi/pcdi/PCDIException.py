'''
Created on 23.01.2022

@author: langwch
'''

from enum import Enum
from datetime import datetime

class PCDI_SEVERITY_LEVELS(Enum):

    WARNING = (1),
    CRITICAL = (2),
    DEBUG = (3),
    INFO = (4)
    
    INVALID = (255)
    
    def __init__(self, val):
        self.val = val
        
    def getId(self):
        return self.val 

    @classmethod
    def fromInteger(cls, v):
        for e in PCDI_SEVERITY_LEVELS:
            if (e.getId() == int(v)):
                return e 
        return PCDI_SEVERITY_LEVELS.INVALID

class PCDIException(object):

    def __init__(self, module_id, exception_id, severity):
        self.module_id = module_id
        self.exception_id = exception_id
        self.severityLevel = PCDI_SEVERITY_LEVELS.fromInteger(severity)
        self.timestamp = datetime.now()
      
    def toDict(self):
        return {"module_id" : self.module_id, "exception_id" : self.exception_id, "severity_level" : str(self.severityLevel), "timestampe" : str(self.timestamp)}
        
    def __str__(self):
        return "PCDIException [" + str(self.module_id) + "." + str(self.exception_id) + ", " + str(self.severityLevel) + "   @ " + str(self.timestamp) + "]"
        
        