'''
Created on 11.04.2020

@author: Christoph
'''

class NoComPortSelectedException(Exception):
    pass

class ComPortAlreadyOpenedException(Exception):
    pass

class ComPortNotOpenedException(Exception):
    pass

class ChecksumError(Exception):
    pass

class InvalidMessageBuildException(Exception):
    pass

class InvalidLengthException(Exception):
    pass

class InvalidArgumentException(Exception):
    pass

class PCDIConnectedException(Exception):
    pass

class BootloaderModeActiveException(Exception):
    pass

class BootloaderModeNotActiveException(Exception):
    pass