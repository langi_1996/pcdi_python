'''
Created on 16.09.2020

@author: christoph
'''
from pcdi.I_PhysicalLayer import I_PhysicalLayer
from pcdi.Exceptions import ComPortAlreadyOpenedException, NoComPortSelectedException
import time
import serial
import serial.tools.list_ports 

class VirtualComPort(I_PhysicalLayer):
    '''
    classdocs
    '''


    def __init__(self, comPort = None):
        '''
        Constructor
        '''
        self.serial = serial.Serial()
        self.comPort = comPort
        
    # ============================================================================================ #
    @classmethod
    def getAvailableComPorts(self):
        """Lists available COM-Ports
                
        https://pyserial.readthedocs.io/en/latest/tools.html#serial.tools.list_ports.ListPortInfo
        
        :returns: available COM-Ports (List<ListPortInfo>)
        """
        return serial.tools.list_ports.comports()
    
    # ============================================================================================ #
    def setComPort(self, comPort):
        """Set COM-Port

        :param comPort: name of comPort (String)

        :returns: void
        """
        self.comPort = comPort
        self.serial.port = comPort
    
    # I_UpSmart_PhysicalLayer Interface Realization   
    # ============================================================================================ # 
    def open(self):
        if(self.comPort is None):
            raise NoComPortSelectedException('No COM-Port is selected to open')
        
        if(self.serial.is_open):
            raise ComPortAlreadyOpenedException('COM-Port ' + str(self.comPort) + 'is already opened')
        
        self.serial.open()
        time.sleep(0.1)
    
    # ============================================================================================ #
    def close(self):
        if(self.serial.is_open):
            self.serial.close()
    
    # ============================================================================================ #
    def isOpen(self):
        return self.serial.is_open

    # ============================================================================================ #
    def switchBaudrate(self, baudrate):
        if(self.serial.is_open):
            self.serial.close()
            self.serial.baudrate = baudrate
            self.serial.open()
        else:
            self.serial.baudrate = baudrate
            
    # ============================================================================================ #
    def configureForPCDIMode(self):
        if(self.comPort is None):
            raise NoComPortSelectedException('No COM-Port is selected to open')
        
        self.serial.port = self.comPort
        self.serial.baudrate = 115200
        self.serial.stopbits = serial.STOPBITS_ONE
        self.serial.parity = serial.PARITY_NONE
        self.serial.bytesize = serial.EIGHTBITS
        self.serial.timeout = 0
        
    # ============================================================================================ #
    def configureForBootloaderMode(self):
        # config serial
        self.serial.port = self.comPort
        self.serial.baudrate = 115200
        self.serial.stopbits = serial.STOPBITS_ONE
        self.serial.parity = serial.PARITY_NONE
        self.serial.bytesize = serial.EIGHTBITS
        self.serial.timeout = 0
    
    # ============================================================================================ #
    def resetCommunicationBuffer(self):
        self.serial.reset_input_buffer()
        self.serial.reset_output_buffer()
    
    # ============================================================================================ #
    def writeAll(self, data):
        self.serial.write(data)
        while(self.serial.out_waiting > 0):
            continue
    
    # ============================================================================================ #
    def readAll(self):
        return self.serial.read_all()

    # ============================================================================================ #
    def numberOfBytesInWaiting(self):
        return self.serial.in_waiting