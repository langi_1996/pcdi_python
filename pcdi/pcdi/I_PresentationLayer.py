'''
Created on 16.09.2020

@author: christoph
'''

from abc import ABC, abstractmethod 

class I_PresentationLayer(ABC):
    
    @abstractmethod
    def connect(self):
        pass
    
    @abstractmethod
    def disconnect(self):
        pass
    
    @abstractmethod
    def isConnected(self):
        pass
    
    @abstractmethod
    def readParameter(self, module_id, parameter_id):
        pass
    
    @abstractmethod
    def writeParameter(self, module_id, parameter_id, new_value):
        pass
    
    @abstractmethod
    def resetParameter(self, module_id, parameter_id):
        pass
    
    @abstractmethod
    def triggerAction(self, action):
        pass
    
    @abstractmethod
    def setCommand(self, command):
        pass
    
    @abstractmethod
    def sendBootloaderMessage(self, command, payload = None):
        pass
    
    @abstractmethod
    def debugRead(self, address, byte_size):
        pass
            
    @abstractmethod
    def registerListener(self, listener):
        pass
    
    @abstractmethod
    def unregisterListener(self, listener):
        pass