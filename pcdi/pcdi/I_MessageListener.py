'''
Created on 16.09.2020

@author: christoph
'''

from abc import ABC, abstractmethod 

class I_MessageListener(ABC):
    '''
    classdocs
    '''
    @abstractmethod
    def notifyListenerMessageReceived(self, msg):
        pass
    
    @abstractmethod
    def notifyListenerMessageSent(self, msg):
        pass
    
    @abstractmethod
    def notifyListenerConnectionChanged(self, connected):
        pass