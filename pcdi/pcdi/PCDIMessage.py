'''
Created on 15.01.2022

@author: langwch
'''

from enum import Enum

from pcdi.Exceptions import InvalidMessageBuildException, InvalidLengthException, ChecksumError

class PCDI_MESSAGE_ID(Enum):
    """
    Enum of all PCDI Message ID's
    """
    HEARTBEAT = (1),
    SUPPORTED_FEATURES = (2),
    DEVICE_TYPE = (3),
    
    COMMAND = (32),
    PARAMETER_GET_INFO = (33),
    PARAMETER_GET_DESCRIPTION = (34),
    PARAMETER_READ = (35),
    PARAMETER_WRITE = (36),
    EXCEPTION_HANDLING = (37),
    EVENT_HANDLING = (38),
    SOFT_SCOPE = (39),
    TRIGGER_ACTION = (40),
    DEBUG_READ = (41),
    DEBUG_WRITE = (42),
    
    BOOTLOADER = (64),

    INVALID = (255)
    def __init__(self, msg_id):
        self.msg_id = msg_id
        
    def getId(self):
        return self.msg_id  
    
    @classmethod
    def fromInteger(cls, value):
        for e in PCDI_MESSAGE_ID:
            if(e.getId() == value or e.getId() + 128 == value):
                return e
        return PCDI_MESSAGE_ID.INVALID
    
    
class PCDIMessage(object):
    '''
    classdocs
    '''


    def __init__(self, dest = 0, source = 0, message_id = PCDI_MESSAGE_ID.INVALID, payload = None):
        '''
        Constructor
        '''
        self.destination_address = dest
        self.source_address = source
        
        self.message_id = message_id
        
        self.payload = payload
        
        if(self.payload is None):
            self.messageLength = 5
        else:
            self.messageLength = len(self.payload) + 5
        
        if(not isinstance(self.message_id, PCDI_MESSAGE_ID)):
            raise TypeError('Expected ' + PCDI_MESSAGE_ID.__name__ + " and got " + self.message_id.__class__.__name__) 
        
    def calculateChecksum(self, slave = False):
        tmp = 0xAA + PCDIMessage.encodeDestinationAndSource(self.destination_address, self.source_address) + self.messageLength + self.message_id.getId()
        if(slave):
            tmp = tmp + 128
        if(self.payload is not None):
            for byte in self.payload:
                tmp = tmp + byte
        return tmp%256
           
    def toByteArray(self):
        msg = []
        # fisrt byte is header 0xAA
        msg.append(0xAA)
        # second byte "Length byte"
        msg.append(self.messageLength)
        # third byte "destination and source"
        msg.append(PCDIMessage.encodeDestinationAndSource(self.destination_address, self.source_address))

        # Third byte "Message ID byte"
        msg.append(self.message_id.getId())
        # payload data
        if(self.payload is not None):
            for byte in self.payload:
                msg.append(byte)
        # checksum
        msg.append(self.calculateChecksum())

        return bytearray(msg)
    
    def __str__(self):

        tmp = self.message_id
            
        tmpStr = ""
        for byte in self.toByteArray():
            tmpStr += " " + hex(byte)
        return "Destination: " + str(self.destination_address) + ", Source: " + str(self.source_address) + " Length: " + str(self.messageLength) + " MessageId: " + str(tmp) + " raw: [" + tmpStr + "]"
    
    @classmethod
    def encodeDestinationAndSource(cls, d, s):
        return (((d<<4)&0xF0) + (s&0xF))
        
    @classmethod
    def fromByteArray(cls, dataBytes):
        if(len(dataBytes) < 5):
            raise InvalidMessageBuildException('Message is too short')
        
        header = dataBytes.pop(0)
        if(header != 0xAA):
            raise InvalidMessageBuildException('First byte is not PCDI-header')
        
        msgLength = int(dataBytes.pop(0))
        
        dest_source_byte = int(dataBytes.pop(0))
        destination = (dest_source_byte&0xF0)>>4
        source = (dest_source_byte&0xF)
        
        msgId = PCDI_MESSAGE_ID.fromInteger(int(dataBytes.pop(0)))
                
        if(msgLength > 32):
            raise InvalidLengthException('Max Message-Length is 32, got: ' + str(msgLength))
        
        if(msgLength > 5):
            msg_payload = []
            for i in range(0,int(msgLength)-5):
                msg_payload.append(int(dataBytes.pop(0)))
        else:
            msg_payload = None
            
        msg = PCDIMessage(dest = destination, source = source, message_id = msgId, payload = msg_payload)  
        
        cs1 = msg.calculateChecksum(slave=True)

        recCS1 = dataBytes.pop(0)

        if(cs1 != recCS1):
            print(msg)
            raise ChecksumError('Checksum error, calc: ' + str(cs1) + ' rec: ' + str(recCS1))
        
        return msg