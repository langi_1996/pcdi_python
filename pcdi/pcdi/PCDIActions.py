'''
Created on 21.01.2022

@author: langwch
'''

from enum import Enum

class PCDI_ACTIONS(Enum):
    """
    Enum of all PCDI Message ID's
    """

    REBOOT = (1),
    PERSITENCE_CLEAR = (2),
    CC_STEP_RESPONSE = (3),
    PWM_OFF = (4),
    PWM_ENABLE = (5),
    ENCODER_OFFSET= (6),
    MOTOR_BEEP = (7),
    SC_STEP_RESPONSE = (8),
    SPEED_CONTROLLER_ON = (9),
    SPEED_CONTROLLER_OFF = (10),
    SET_HOME = (11),
    POSITION_CONTROL_ON = (12),
    POSITION_CONTROL_OFF = (13),
    MOVE_OPEN = (14),
    MOVE_CLOSE = (15),
    DO_LEARNING = (16),
    CHIRP = (17),
    JUMP_TO_BOOTLOADER = (20),
    INVALID = (255)
    
    def __init__(self, val):
        self.val = val
        
    def getId(self):
        return self.val 

    @classmethod
    def fromInteger(cls, v):
        for e in PCDI_ACTIONS:
            if (e.getId() == int(v)):
                return e 
        return PCDI_ACTIONS.INVALID