'''
Created on 15.01.2022

@author: langwch
'''

from enum import Enum

class PCDI_UNITS(Enum):
    """
    Enum of all PCDI Unit ID's
    """
    NONE = 0, "",
    AMPERE = 1, "A",
    VOLT = 2, "V",
    RAD = 3, "rad",
    RAD_S = 4, "rad/s",
    OHM = 5, "Ohm",
    HENRY = 6, "H",
    HERTZ = 7, "Hz",
    WATT = 8, "W",
    METER = 9, "m",
    SECONDS = 10, "s",
    VOLT_S = 11, "Vs",
    NEWTON_METER = 12, "Nm",
    INTERIA = 13, "kg/m/s^2",
    
    INVALID = 255, ""
    
    def __init__(self, val, representation):
        self.val = val
        self.representation = representation
        
    def getId(self):
        return self.val 
    
    def getString(self):
        return self.representation
    
    @classmethod
    def fromInteger(cls, value):
        for e in PCDI_UNITS:
            if(e.getId() == value):
                return e
        return PCDI_UNITS.INVALID