'''
Created on 16.01.2022

@author: langwch
'''
from enum import Enum

class PCDI_PERMISSION_FLAGS(Enum):

    READONLY = (0x1),
    WRITEABLE = (0x2),
    PERSISTENT = (0x4),
    CONST = (0x8),
    LOAD_DEFAULT = (0x10)

    
    INVALID = (256)
    
    def __init__(self, val):
        self.val = val
        
    def getShift(self):
        return self.val 

    @classmethod
    def fromInteger(cls, v):
        result = []
        for e in PCDI_PERMISSION_FLAGS:
            if (int(v)&e.getShift()):
                result.append(e)
        return result