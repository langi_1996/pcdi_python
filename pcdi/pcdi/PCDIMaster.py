'''
Created on 15.01.2022

@author: langwch
'''

import threading
import copy
from enum import Enum
from datetime import datetime

from pcdi.I_TransportationLayer import I_TransporationLayer
from pcdi.PCDIMessage import PCDIMessage, PCDI_MESSAGE_ID
from pcdi.I_MessageListener import I_MessageListener
from pcdi.Exceptions import InvalidArgumentException, ChecksumError, InvalidLengthException
from pcdi.PCDIStatistic import PCDIStatistic

from util.StopableThread import StopableThread


class PCDI_MASTER_COM_STATE(Enum):
    """
    doc
    """
    SEND = 1,
    RECEIVE = 2,
    WAIT_FOR_BOOTLOADER = 3
    
class PCDI_MASTER_MODE(Enum):
    """
    doc
    """
    NORMAL = 1,
    BOOTLOADER = 2

class PCDIMaster(I_TransporationLayer):
    '''
    classdocs
    '''
    

    def __init__(self, physical_layer, cycle_time = 0.001, max_resend_retries = 5):
        '''
        Constructor
        '''
        
        self.physical_layer = physical_layer
        self.listener = []
        
        self.RxTxThread = None
        self.rxByteBuffer = []
        self.txMessageBuffer = []
        self.lock = threading.RLock()
        
        self.mode = PCDI_MASTER_MODE.NORMAL
        
        self.LastSentMessage = None
        
        self.rxtxCycleTime = cycle_time # 1ms        
        self.receiveTimout_ms = 1000
        self.stateMaxTimeout = 0
        self.stateCounter = 0
        #message timeout handling
        self.maxResendRetries = max_resend_retries
        self.resendRetryCounter = 0
        self.state = PCDI_MASTER_COM_STATE.SEND
        self.m_state = PCDI_MASTER_COM_STATE.SEND
        
        
        self.heartbeat_send_interval_s = 1
        dt = datetime.now()
        self.last_heartbeat_sent_ts = datetime.timestamp(dt)
        
        self.heartbeatTxCounterMaximum = int(1/self.rxtxCycleTime)
        self.heartbeatRxCounterMaximum = 2*self.heartbeatTxCounterMaximum
        
        self.heartbeatRxCnt = 0
        self.heartbeatTxCnt = self.heartbeatTxCounterMaximum
        self.automaticHeartbeatEnabled = True
        
        self.isConnected = False
        self.doingLogin = False
         
        self.notifyListenerAtHeartbeat = False
        
        self.statistics = PCDIStatistic()
        
        #module version
        self.version = '1.0'
        """ module version """
        self.buildDate = '16.06.2023'
        """ date of build """
        self.buildTime = '10:00'
        """ time of build """
        
    def connect(self, bootloader_mode = False):
        if(self.isConnected or self.doingLogin):
            print("already connected or doing login")
            return
        
        if(bootloader_mode):
            self.mode = PCDI_MASTER_MODE.BOOTLOADER
            self.automaticHeartbeatEnabled = False
        else:
            self.mode = PCDI_MASTER_MODE.NORMAL
        
        self.doingLogin = True
        
        self.statistics.resetRxTx()
        
        self.txMessageBuffer.clear()
        
        self.physical_layer.configureForPCDIMode()
        self.physical_layer.open()
        self.physical_layer.resetCommunicationBuffer()
        
        self.stateMaxTimeout = self.receiveTimout_ms/1000.0/self.rxtxCycleTime
        self.stateCounter = 0
        self.resendRetryCounter = 0
        
        self.heartbeatRxCnt = 0
        self.heartbeatTxCnt = self.heartbeatTxCounterMaximum
        
        # start rxtx-Thread
        if(self.RxTxThread is None):
            #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.INFO, "UpSmartMaster RxTxThread started"))
            
            if(self.mode == PCDI_MASTER_MODE.BOOTLOADER):
                self.state = PCDI_MASTER_COM_STATE.WAIT_FOR_BOOTLOADER
                self.m_state = PCDI_MASTER_COM_STATE.WAIT_FOR_BOOTLOADER
            else:
                self.state = PCDI_MASTER_COM_STATE.SEND
                self.m_state = PCDI_MASTER_COM_STATE.SEND
            
            
            self.RxTxThread = StopableThread(delay=self.rxtxCycleTime, execute=self.RxTxHandler)
            self.RxTxThread.start()
        else:
            print("something went wrong, rxtx-thread is still running")
    
    def disconnect(self):
        if(self.RxTxThread is not None):
            #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.INFO, "UpSmartMaster RxTxThread stopped"))
            self.RxTxThread.stop()
            self.RxTxThread = None
        if(self.physical_layer.isOpen()):
            self.physical_layer.close()
            
        self.isConnected = False
        self.doingLogin = False
        self.notifyAllListenerConnectionChanged()

    def isConnected(self):
        return self.isConnected
    
    def enableNotifyListenerAtHeartbeat(self):
        return 
    
    def disableNotifyListenerAtHeartbeat(self):
        return 
    
    def sendMessage(self, message):
        if(not isinstance(message, PCDIMessage)):
            raise InvalidArgumentException('Invalid Argument, expected type UpSmartMessage')
        
        self.lock.acquire(blocking=True)
        self.txMessageBuffer.append(copy.deepcopy(message))
        self.lock.release()
    
    def getStatistics(self):
        return
    
    def sendHeartbeatMessage(self):
        msg = PCDIMessage(dest = 2, source = 1, message_id = PCDI_MESSAGE_ID.HEARTBEAT)
        #print(msg)
        self.physical_layer.writeAll(msg.toByteArray())
        #print("sent heartbeat")
    
    def RxTxHandler(self):
        if(self.state == PCDI_MASTER_COM_STATE.SEND):
            #handle heartbeat
            dt = datetime.now()
            ts = datetime.timestamp(dt)
            delta = ts - self.last_heartbeat_sent_ts
            
            if(self.automaticHeartbeatEnabled and delta >= self.heartbeat_send_interval_s):
                self.sendHeartbeatMessage()
                self.statistics.increaseTotalMessagesTx()
                self.last_heartbeat_sent_ts = ts
                self.state = PCDI_MASTER_COM_STATE.RECEIVE
            else:
                #send from tx buffer  
                self.lock.acquire(blocking=True)
                self.statistics.setWaitingMessagesTx(len(self.txMessageBuffer))
                if(len(self.txMessageBuffer) > 0):
                    txmsg = self.txMessageBuffer.pop(0)
                    
                    #print(txmsg)
                    
                    self.physical_layer.writeAll(txmsg.toByteArray())
                    

                    self.LastSentMessage = txmsg 
                
                    self.notifyAllListenerMessageSent(txmsg)
                    
                    self.statistics.increaseTotalMessagesTx()
                    #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.DEBUG, "tx: " + str(msg)))
                    self.state = PCDI_MASTER_COM_STATE.RECEIVE
                self.lock.release()
                
                
        elif(self.state == PCDI_MASTER_COM_STATE.RECEIVE):
            self.stateCounter = self.stateCounter + 1
            #parse from rx buffer
            if(self.physical_layer.numberOfBytesInWaiting() > 0):
                for byte in self.physical_layer.readAll(): 
                    self.rxByteBuffer.append(byte)
                #print(self.rxByteBuffer) # debug
            #look for device ID header
            while(len(self.rxByteBuffer) > 0 and self.rxByteBuffer[0] != 0xAA):
                #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.DEBUG, str(self.rxByteBuffer[0]) + " != " + str(UpSmartMessage.encodeID(1))))
                print("0xAA != " + hex(self.rxByteBuffer[0]))
                self.rxByteBuffer.pop(0)
    
            availableBytes = len(self.rxByteBuffer)
            if(availableBytes >= 5):
                msgLength = self.rxByteBuffer[1]
                if(availableBytes >= msgLength):
                    msgBytes = []
                    for i in range(0, int(msgLength)):
                        msgBytes.append(self.rxByteBuffer.pop(0))
                    try:
                        #print(msgBytes) #debug
                        msg = PCDIMessage.fromByteArray(msgBytes)
                        self.statistics.increaseTotalMessagesRx()
                                                   
                        #print(msg) #debug
                                                    
                        self.messageHandler(msg)
                        
                        # if valid message has been received, reset resend retry counter
                        self.resendRetryCounter = 0
                        
                    except (ChecksumError, InvalidLengthException) as ex:
                        self.statistics.increaseCorruptedMessages()
                        print(ex)
                        #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.ERROR, str(ex)))
                        pass
                    finally:
                        self.state = PCDI_MASTER_COM_STATE.SEND
                  
            if(self.stateCounter > self.stateMaxTimeout):
                self.state = PCDI_MASTER_COM_STATE.SEND
                
                #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.ERROR, "Receive timeout!"))
                print("PCDIMaster: No answer from device!" + str(self.LastSentMessage))
                
                #retry
                self.lock.acquire(blocking=True)
                if(self.LastSentMessage is not None):
                    self.txMessageBuffer.insert(0, self.LastSentMessage)
                self.lock.release()
                
                self.resendRetryCounter = self.resendRetryCounter + 1
                if(self.mode == PCDI_MASTER_MODE.NORMAL and self.resendRetryCounter > self.maxResendRetries):
                    self.disconnect()
                    self.resendRetryCounter = 0
                    #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.ERROR, "Maximum Resend Retries exceeded, connection timeout!"))
                    print("PCDIMaster: Maximum Resend Retries exceeded, connection timeout!")
                
                #self.heartbeatTxCnt = self.heartbeatTxCounterMaximum
                
                
        elif(self.state == PCDI_MASTER_COM_STATE.WAIT_FOR_BOOTLOADER):
            if(self.physical_layer.numberOfBytesInWaiting() > 0):
                for byte in self.physical_layer.readAll(): 
                    self.rxByteBuffer.append(byte)
                
            #look for device ID header
            while(len(self.rxByteBuffer) > 0 and self.rxByteBuffer[0] != 0xAA):
                #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.DEBUG, str(self.rxByteBuffer[0]) + " != " + str(UpSmartMessage.encodeID(1))))
                self.rxByteBuffer.pop(0)
    
            availableBytes = len(self.rxByteBuffer)
            if(availableBytes >= 5):
                msgLength = self.rxByteBuffer[1]
                if(availableBytes >= msgLength):
                    msgBytes = []
                    for i in range(0, int(msgLength)):
                        msgBytes.append(self.rxByteBuffer.pop(0))
                    try:
                        #print(msgBytes)
                        msg = PCDIMessage.fromByteArray(msgBytes)
                        self.statistics.increaseTotalMessagesRx()
                                                    
                        self.messageHandler(msg)
                        
                        self.state = PCDI_MASTER_COM_STATE.SEND
                        self.automaticHeartbeatEnabled = True
                        print("got boot-up message from bootloader")
                        
                        msg = PCDIMessage(dest = 2, source = 1, message_id = PCDI_MESSAGE_ID.BOOTLOADER, payload=[1])
                        self.physical_layer.writeAll(msg.toByteArray())
                        
                    except (ChecksumError, InvalidLengthException) as ex:
                        self.statistics.increaseCorruptedMessages()
                        print(ex)
                        #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.ERROR, str(ex)))
                        pass

         
         
        if(self.m_state is not self.state):
            self.stateCounter = 0
        
        self.m_state = self.state
        #print(self.state)
           
        if(self.automaticHeartbeatEnabled): 
            if(self.heartbeatRxCnt >= self.heartbeatRxCounterMaximum):
                #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.ERROR, "Heartbeat timeout"))
                print("Heartbeat timeout!")
                self.disconnect()
                self.statistics.increaseHeartbeatTimeouts()
            
            self.heartbeatRxCnt = self.heartbeatRxCnt + 1  

    def messageHandler(self, msg):

        if(not isinstance(msg, PCDIMessage)):
            return
        
        #self.logger.addLogMessage(LoggerMessage(LOG_TYPE.DEBUG, "rx: " + str(msg)))
        
        #print(msg)
        
        #handle heartbeat seperately
        if(msg.message_id == PCDI_MESSAGE_ID.HEARTBEAT):
            #print("got heartbeat")
            #reset heartbeat
            self.heartbeatRxCnt = 0
            if(self.notifyListenerAtHeartbeat):
                self.notifyAllListenerMessageReceived(msg)
                
            if(self.isConnected == False):
                self.doingLogin = False
                self.isConnected = True
                self.notifyAllListenerConnectionChanged()
        else:
            self.notifyAllListenerMessageReceived(msg)

    def registerListener(self, listener):
        if(not isinstance(listener, I_MessageListener)):
            return
        if(listener in self.listener):
            return
        self.listener.append(listener)
    
    def unregisterListener(self, listener):
        if(listener in self.listener):
            self.listener.remove(listener) 
            
    def notifyAllListenerMessageReceived(self, msg):
        for listener in self.listener:
            listener.notifyListenerMessageReceived(msg)
            
    def notifyAllListenerMessageSent(self, msg):
        for listener in self.listener:
            listener.notifyListenerMessageSent(msg)
            
    def notifyAllListenerConnectionChanged(self):
        for listener in self.listener:
            listener.notifyListenerConnectionChanged(self.isConnected)