'''
Created on 21.01.2022

@author: langwch
'''

from enum import Enum

class PCDI_SOFTSCOPE_COMMAND(Enum):
    """
    Enum of all PCDI Message ID's
    """

    GET_STATE = (0),
    RESET = (1),
    CONFIG = (2),
    CONFIG_CHANNEL = (64),
    READ_CHANNEL_CONFIG = (65),
    READ_CONFIG = (66),
    READ_CHANNEL = (128),
    
    INVALID = (255)
    
    def __init__(self, val):
        self.val = val
        

    @classmethod
    def fromInteger(cls, v):
        for e in PCDI_SOFTSCOPE_COMMAND:
            if (e.val == int(v)):
                return e 
        return PCDI_SOFTSCOPE_COMMAND.INVALID