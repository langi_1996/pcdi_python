'''
Created on 16.09.2020

@author: christoph
'''

from abc import ABC, abstractmethod 


class I_PhysicalLayer(ABC):
    '''
    classdocs
    '''

    @abstractmethod
    def open(self):
        pass
    
    @abstractmethod
    def close(self):
        pass
    
    @abstractmethod
    def isOpen(self):
        pass

    @abstractmethod
    def switchBaudrate(self, baudrate):
        pass
    
    @abstractmethod
    def configureForPCDIMode(self):
        pass
    
    @abstractmethod
    def configureForBootloaderMode(self):
        pass
    
    @abstractmethod
    def resetCommunicationBuffer(self):
        pass
    
    @abstractmethod
    def writeAll(self, data):
        pass
    
    @abstractmethod
    def readAll(self):
        pass

    @abstractmethod
    def numberOfBytesInWaiting(self):
        pass