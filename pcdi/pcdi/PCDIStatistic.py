'''
Created on 29.06.2020

@author: christoph
'''

class PCDIStatistic(object):
    '''
    classdocs
    '''

    def __init__(self):
        '''
        Constructor
        '''
        self.reset()
    
    # ============================================================================================ #
    def reset(self):
        self.totalMessagesRx = 0
        """ total messages received, for statistics """
        self.totalMessagesTx = 0
        """ total messages sent, for statistics """ 
        self.corruptedMessages = 0
        """ total messages with checksum errors or invalid length""" 
        self.logins = 0
        """ total count of logins """ 
        self.heartbeatTimeouts = 0
        """ total count of timeouts detected """ 
        self.waitingMessagesTx = 0
        """ wating messages in tx queue """ 
    
    # ============================================================================================ #
    def increaseTotalMessagesTx(self):
        self.totalMessagesTx = self.totalMessagesTx + 1
    
    # ============================================================================================ # 
    def increaseTotalMessagesRx(self):
        self.totalMessagesRx = self.totalMessagesRx + 1
     
    # ============================================================================================ #   
    def increaseCorruptedMessages(self):
        self.corruptedMessages = self.corruptedMessages + 1
        
    # ============================================================================================ #
    def increaseLogins(self):
        self.logins = self.logins + 1
    
    # ============================================================================================ #   
    def increaseHeartbeatTimeouts(self):
        self.heartbeatTimeouts = self.heartbeatTimeouts + 1
    
    # ============================================================================================ #   
    def setWaitingMessagesTx(self, nrMessages):
        self.waitingMessagesTx = nrMessages
        
    def resetRxTx(self):
        self.totalMessagesRx = 0
        self.totalMessagesTx = 0
        self.corruptedMessages = 0
    # ============================================================================================ #    
    def __str__(self):
        return "totalRx: " + str(self.totalMessagesRx) + ", totalTx: " + str(self.totalMessagesTx) + ", waitTx: " + str(self.waitingMessagesTx) + ", corrupted: " + str(self.corruptedMessages) + ", logins: " + str(self.logins) + ", timeouts: " + str(self.heartbeatTimeouts)
        