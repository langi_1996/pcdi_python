'''
Created on 15.01.2022

@author: langwch
'''

from enum import Enum

class PCDI_DATATYPES(Enum):
    """
    Enum of all PCDI Message ID's
    """

    FLOAT = (1),
    INTEGER_32BIT = (2),
    INTEGER_16BIT = (3),
    INTEGER_8BIT = (4),
    
    INVALID = (255)
    
    def __init__(self, val):
        self.val = val
        
    def getId(self):
        return self.val 

    @classmethod
    def fromInteger(cls, v):
        for e in PCDI_DATATYPES:
            if (e.getId() == int(v)):
                return e 
        return PCDI_DATATYPES.INVALID