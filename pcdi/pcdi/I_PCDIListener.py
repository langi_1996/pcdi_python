'''
Created on 15.01.2022

@author: langwch
'''

from abc import ABC, abstractmethod 

class I_PCDIListener(ABC):

    @abstractmethod
    def notifyConnectionChanged(self, isConnected):
        pass
    
    @abstractmethod
    def notifyParameterReceived(self, parameter):
        pass

    @abstractmethod
    def notifyEvent(self, event):
        pass
    
    @abstractmethod
    def notifyCommandDone(self):
        pass
    
    @abstractmethod
    def notifySoftScopeCommandDone(self, softscope_cmd, triggered = None, done = None, channel = None, index = None, result = None, module_id = None, parameter_id = None, gain = None):
        pass
    
    @abstractmethod
    def notifyBootloaderCommandDone(self, bootloader_cmd, payload = None):
        pass
    
    @abstractmethod
    def notifyDebugReadDone(self, address, byte_size, data):
        pass