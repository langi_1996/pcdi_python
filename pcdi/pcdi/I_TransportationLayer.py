'''
Created on 16.09.2020

@author: christoph
'''

from abc import ABC, abstractmethod 

class I_TransporationLayer(ABC):
    '''
    classdocs
    '''

    @abstractmethod
    def connect(self):
        pass

    @abstractmethod
    def disconnect(self):
        pass
    
    @abstractmethod
    def isConnected(self):
        pass
        
    @abstractmethod
    def enableNotifyListenerAtHeartbeat(self):
        pass
    
    @abstractmethod
    def disableNotifyListenerAtHeartbeat(self):
        pass
        
    @abstractmethod
    def sendMessage(self, message):
        pass   

    @abstractmethod
    def getStatistics(self):
        pass    

    @abstractmethod
    def registerListener(self, listener):
        pass
    
    @abstractmethod
    def unregisterListener(self, listener):
        pass