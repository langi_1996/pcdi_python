'''
Created on 15.01.2022

@author: langwch
'''

from pcdi.PCDIDatatypes import PCDI_DATATYPES
from pcdi.PCDIUnits import PCDI_UNITS

class Parameter(object):
    '''
    classdocs
    '''


    def __init__(self, module_id = 0, parameter_id = 0, data_type = PCDI_DATATYPES.INVALID, unit = PCDI_UNITS.NONE, description = ""):
        '''
        Constructor
        '''
        self.module_id = module_id
        self.parameter_id = parameter_id
        self.data_type = data_type
        self.unit = unit
        self.description = description
        
        self.permission_flags = 0
        self.default_value = 0
        self.value = 0
        
    def setValue(self, value):
        self.value = value
        
    def __str__(self):
        return str(self.module_id) + "." + str(self.parameter_id) + " " + str(self.description) + " [" + str(self.value) + " " + str(self.unit.getString()) + ", DT: " + str(self.data_type) + ", " + str(self.permission_flags) + ", default = " + str(self.default_value) + "]"