'''
Created on 15.01.2022

@author: langwch
'''

from pcdi.I_MessageListener import I_MessageListener
from pcdi.I_PresentationLayer import I_PresentationLayer
from pcdi.PCDIMessage import PCDI_MESSAGE_ID, PCDIMessage
from pcdi.PCDIDatatypes import PCDI_DATATYPES
from pcdi.Parameter import Parameter
from pcdi.PCDIPermissionFlags import PCDI_PERMISSION_FLAGS
from pcdi.PCDISoftScopeCommands import PCDI_SOFTSCOPE_COMMAND
from pcdi.PCDIUnits import PCDI_UNITS
from pcdi.I_PCDIListener import I_PCDIListener
from pcdi.PCDIException import PCDIException

from util.StopableThread import StopableThread

import struct

from bootloader.bootloader import *

class PCDIParser(I_PresentationLayer, I_MessageListener):

    def __init__(self, transportation_layer):

        self.transportation_layer = transportation_layer
        self.transportation_layer.registerListener(self)
        
        self.listener = []
        
        self.parameterList = []
        
        self.cyclicThread = None
        self.cyclicInterval_s = 1

    def cyclic(self):
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.EXCEPTION_HANDLING, dest = 2, source = 1)
        self.transportation_layer.sendMessage(msg)

    def notifyListenerMessageReceived(self, msg):
        
        if(msg.message_id == PCDI_MESSAGE_ID.PARAMETER_READ):
            
            module_id = msg.payload[0]
            parameter_id = msg.payload[1]
            data_type = PCDI_DATATYPES.fromInteger(msg.payload[2])
            
            par = self.findParameterInList(module_id, parameter_id)
            if(par is None):
                par = Parameter(module_id = module_id, parameter_id = parameter_id, data_type = data_type)
                self.parameterList.append(par)
                
            par.data_type = data_type
            rec_value = 0
            
            if(par.data_type == PCDI_DATATYPES.FLOAT):
                value = [msg.payload[3], msg.payload[4], msg.payload[5], msg.payload[6]]
                tmp = struct.unpack(">f", bytearray(value))
                rec_value = tmp[0]
                
            elif(par.data_type == PCDI_DATATYPES.INTEGER_32BIT):
                value = [msg.payload[3], msg.payload[4], msg.payload[5], msg.payload[6]]
                tmp = struct.unpack(">L", bytearray(value))
                rec_value = tmp[0]

            elif(par.data_type == PCDI_DATATYPES.INTEGER_16BIT):
                value = [msg.payload[3], msg.payload[4]]
                tmp = struct.unpack(">H", bytearray(value))
                rec_value = tmp[0]

            elif(par.data_type == PCDI_DATATYPES.INTEGER_8BIT):
                rec_value = int.from_bytes([msg.payload[3]], byteorder='big')
                
            par.setValue(rec_value)
            
            for l in self.listener:
                l.notifyParameterReceived(par)
        
        elif(msg.message_id == PCDI_MESSAGE_ID.PARAMETER_GET_INFO):
            module_id = msg.payload[0]
            parameter_id = msg.payload[1]
            permission_flags = PCDI_PERMISSION_FLAGS.fromInteger(msg.payload[2])
            data_type = PCDI_DATATYPES.fromInteger(msg.payload[3])
            par_unit = PCDI_UNITS.fromInteger(msg.payload[4])
            tmp = [msg.payload[5], msg.payload[6], msg.payload[7], msg.payload[8]]
            
            vf = struct.unpack(">f", bytearray(tmp))
            par_default = vf[0]
            
            par = self.findParameterInList(module_id, parameter_id)
            if(par is None):
                par = Parameter(module_id = module_id, parameter_id = parameter_id, data_type = data_type)
                self.parameterList.append(par)
            par.permission_flags = permission_flags
            par.unit = par_unit
            par.default_value = par_default
            
        elif(msg.message_id == PCDI_MESSAGE_ID.PARAMETER_GET_DESCRIPTION):
            module_id = msg.payload[0]
            parameter_id = msg.payload[1]
            
            par = self.findParameterInList(module_id, parameter_id)
            if(par is None):
                par = Parameter(module_id = module_id, parameter_id = parameter_id)
                self.parameterList.append(par)
            par.description = "" 
            for i in range(2,len(msg.payload)):
                par.description = par.description + chr(msg.payload[i])
                
        elif(msg.message_id == PCDI_MESSAGE_ID.SOFT_SCOPE):
            cmd = PCDI_SOFTSCOPE_COMMAND.fromInteger(msg.payload[0])
            
            if(cmd == PCDI_SOFTSCOPE_COMMAND.GET_STATE):
                d = msg.payload[1]
                t = msg.payload[2]
                for l in self.listener:
                    l.notifySoftScopeCommandDone(PCDI_SOFTSCOPE_COMMAND.GET_STATE, done = d, triggered = t)
                    
            elif(cmd == PCDI_SOFTSCOPE_COMMAND.RESET):
                for l in self.listener:
                    l.notifySoftScopeCommandDone(PCDI_SOFTSCOPE_COMMAND.RESET)
                    
            elif(cmd == PCDI_SOFTSCOPE_COMMAND.CONFIG):
                for l in self.listener:
                    l.notifySoftScopeCommandDone(PCDI_SOFTSCOPE_COMMAND.CONFIG)
                            
            elif(cmd == PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL):
                for l in self.listener:
                    l.notifySoftScopeCommandDone(PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL)
                    
            elif(cmd == PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL_CONFIG):
                if(len(msg.payload) >= 7):
                    channel_r = msg.payload[1]
                    module_id_r = msg.payload[2]
                    parameter_id_r = msg.payload[3]
                    value = [msg.payload[4], msg.payload[5], msg.payload[6], msg.payload[7]]
                    gain_r = struct.unpack(">f", bytearray(value))
                    for l in self.listener:
                        l.notifySoftScopeCommandDone(PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL, channel = channel_r, module_id = module_id_r, parameter_id = parameter_id_r, gain = gain_r)
            
            elif(cmd == PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL):
                c = msg.payload[1]
                value = [msg.payload[2], msg.payload[3]]
                tmp = struct.unpack(">H", bytearray(value))
                i = tmp[0]
                value = [msg.payload[4], msg.payload[5], msg.payload[6], msg.payload[7]]
                tmp = struct.unpack(">f", bytearray(value))
                v = tmp[0]
                
                for l in self.listener:
                    l.notifySoftScopeCommandDone(PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL, channel = c, index = i, result = v)
                
        elif(msg.message_id == PCDI_MESSAGE_ID.EXCEPTION_HANDLING):
            if(msg.payload is None):
                return
            number_of_exceptions = int(len(msg.payload)/3)
            for i in range(0,number_of_exceptions):
                ex = PCDIException(msg.payload[i*3], msg.payload[i*3+1], msg.payload[i*3+2])
                for l in self.listener:
                    l.notifyEvent(ex)
                    
            if(number_of_exceptions >= 3):
                msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.EXCEPTION_HANDLING, dest = 2, source = 1)
                self.transportation_layer.sendMessage(msg)
                
        elif(msg.message_id == PCDI_MESSAGE_ID.BOOTLOADER):
            if(msg.payload is None):
                return
            
            bootloader_cmd_id = msg.payload[0]
            
            bootloader_payload = []
            for i in range(1, len(msg.payload)):
                bootloader_payload.append(msg.payload[i])
            
            for l in self.listener:
                l.notifyBootloaderCommandDone(BOOTLOADER_COMMAND.fromInteger(bootloader_cmd_id), bootloader_payload)
                
        elif(msg.message_id == PCDI_MESSAGE_ID.DEBUG_READ):
            if(msg.payload is None):
                return
            
            if(len(msg.payload) < 5):
                return
            
            bytes_read = msg.payload[0]
            address = (msg.payload[1] << 24) + (msg.payload[2] << 16) + (msg.payload[3] << 8) + (msg.payload[4])
        
            data = []
            for i in range(0, bytes_read):
                data.append(msg.payload[5+i])
            
            for l in self.listener:
                l.notifyDebugReadDone(address, bytes_read, data)
            
        elif(msg.message_id == PCDI_MESSAGE_ID.TRIGGER_ACTION):
            for l in self.listener:
                l.notifyCommandDone()
        
    def notifyListenerMessageSent(self, msg):
        pass
    
    def notifyListenerConnectionChanged(self, connected):
        if(connected == False):
            self.parameterList = []  
            
        for l in self.listener:
            l.notifyConnectionChanged(connected) 

        if(connected):
            if(self.cyclicThread is None):
                self.cyclicThread = StopableThread(delay=self.cyclicInterval_s, execute=self.cyclic)
                self.cyclicThread.start()
        else:
            if(self.cyclicThread is not None):
                self.cyclicThread.stop()
                self.cyclicThread = None

    def connect(self, bootloader_mode = False):
        self.transportation_layer.connect(bootloader_mode)
    
    def disconnect(self):
        self.transportation_layer.disconnect()
    
    def isConnected(self):
        return self.transportation_layer.isConnected
    
    def readParameter(self, module_id, parameter_id):
        if(module_id >= 255 or parameter_id >= 255):
            return #TODO: throw exception
        
        par = self.findParameterInList(module_id, parameter_id)
        if(par is None):
            self.requestParameterInfoAndDescription(module_id, parameter_id)
        
        msg_payload = [module_id, parameter_id]
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.PARAMETER_READ, dest = 2, source = 1, payload = msg_payload)
        self.transportation_layer.sendMessage(msg)
    
    def writeParameter(self, module_id, parameter_id, new_value):
        if(module_id >= 255 or parameter_id >= 255):
            return #TODO: throw exception
        
        par = self.findParameterInList(module_id, parameter_id)
        if(par is None):
            return
        
        valueBytes = []
        if(par.data_type == PCDI_DATATYPES.FLOAT):
            valueBytes = struct.pack(">f", new_value)
            
        elif(par.data_type == PCDI_DATATYPES.INTEGER_32BIT):
            valueBytes = struct.pack(">L", int(new_value))
        
        elif(par.data_type == PCDI_DATATYPES.INTEGER_16BIT):
            valueBytes = struct.pack(">H", int(new_value))
        
        elif(par.data_type == PCDI_DATATYPES.INTEGER_8BIT):
            valueBytes = struct.pack(">b", int(new_value))     
        
        msg_payload = [module_id, parameter_id, par.data_type.getId()]
        
        for b in valueBytes:
            msg_payload.append(b)
        
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.PARAMETER_WRITE, dest = 2, source = 1, payload = msg_payload)
        
        self.transportation_layer.sendMessage(msg)
    
    def resetParameter(self, module_id, parameter_id):
        pass
    
    def triggerAction(self, action):
        action_bytes = struct.pack(">H", action.getId())
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.TRIGGER_ACTION, dest = 2, source = 1, payload = [action_bytes[0], action_bytes[1]])
        self.transportation_layer.sendMessage(msg)
    
    def setCommand(self, command):
        pass
    
    def sendScopeCommand(self, scope_command, undersample = None, trigger_level = None, gain = None, channel = None, module_id = None, param_id = None, index = None):
        
        if(scope_command == PCDI_SOFTSCOPE_COMMAND.GET_STATE):
            msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.SOFT_SCOPE, dest = 2, source = 1, payload = [PCDI_SOFTSCOPE_COMMAND.GET_STATE.val])
            self.transportation_layer.sendMessage(msg)
        
        elif(scope_command == PCDI_SOFTSCOPE_COMMAND.RESET):
            msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.SOFT_SCOPE, dest = 2, source = 1, payload = [PCDI_SOFTSCOPE_COMMAND.RESET.val])
            self.transportation_layer.sendMessage(msg)
            
        elif(scope_command == PCDI_SOFTSCOPE_COMMAND.CONFIG):
            if ((undersample is not None) and (trigger_level is not None)):
                undersample_bytes = struct.pack(">H", int(undersample))
                trigger_level_bytes = struct.pack(">f", trigger_level)
                msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.SOFT_SCOPE, dest = 2, source = 1, payload = [PCDI_SOFTSCOPE_COMMAND.CONFIG.val, undersample_bytes[0], undersample_bytes[1], trigger_level_bytes[0], trigger_level_bytes[1], trigger_level_bytes[2], trigger_level_bytes[3]])
                self.transportation_layer.sendMessage(msg)
                
        elif(scope_command == PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL):
            if ((channel is not None) and (gain is not None) and (param_id is not None) and (module_id is not None)):
                gain_bytes = struct.pack(">f", gain)
                msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.SOFT_SCOPE, dest = 2, source = 1, payload = [PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL.val, channel, module_id, param_id, gain_bytes[0], gain_bytes[1], gain_bytes[2], gain_bytes[3]])
                self.transportation_layer.sendMessage(msg)
                
        elif(scope_command == PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL):
            if ((channel is not None) and (index is not None)):
                index_bytes = struct.pack(">H", int(index))
                msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.SOFT_SCOPE, dest = 2, source = 1, payload = [PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL.val, channel, index_bytes[0], index_bytes[1]])
                self.transportation_layer.sendMessage(msg)
        
        elif(scope_command == PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL_CONFIG):
            if (channel is not None):
                msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.SOFT_SCOPE, dest = 2, source = 1, payload = [PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL_CONFIG.val, channel])
                self.transportation_layer.sendMessage(msg)
    
    def registerListener(self, listener):
        if(not isinstance(listener, I_PCDIListener)):
            return
        if(listener in self.listener):
            return
        self.listener.append(listener)
    
    def unregisterListener(self, listener):
        if(listener in self.listener):
            self.listener.remove(listener) 
    
    def findParameterInList(self, module_id, parameter_id):
        for par in self.parameterList:
            if(par.module_id == module_id and par.parameter_id == parameter_id):
                return par
        return None
    
    def requestParameterInfoAndDescription(self, module_id, parameter_id):
        msg_payload = [module_id, parameter_id]
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.PARAMETER_GET_INFO, dest = 2, source = 1, payload = msg_payload)
        self.transportation_layer.sendMessage(msg)
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.PARAMETER_GET_DESCRIPTION, dest = 2, source = 1, payload = msg_payload)
        self.transportation_layer.sendMessage(msg)
        
    def sendBootloaderMessage(self, command, payload = None):
        msg_payload = [command.getId()]
        if(payload is not None):
            for x in payload:
                msg_payload.append(x)
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.BOOTLOADER, dest = 2, source = 1, payload = msg_payload)
        #print(msg)
        self.transportation_layer.sendMessage(msg)
        
    def debugRead(self, address, byte_size):
        msg_payload = [byte_size, (address>>24)&0xFF, (address>>16)&0xFF, (address>>8)&0xFF, address&0xFF]
        msg = PCDIMessage(message_id = PCDI_MESSAGE_ID.DEBUG_READ, dest = 2, source = 1, payload = msg_payload)
        self.transportation_layer.sendMessage(msg)
        
        
        