'''
Created on 26.08.2020

@author: christoph
'''

from util.Logger import Logger, LoggerMessage, LOG_TYPE
import tkinter as tk
import threading

class LoggerGUIWindow(threading.Thread):
    '''
    classdocs
    '''
    
    def __init__(self):
        threading.Thread.__init__(self)
        
        self.logger = Logger.getInstance()
        self.running = False
        
        self.start()
        

    def callback(self):
        self.root.quit()

    def run(self):
        self.root = tk.Tk()
        self.root.title("Logger GUI")
        self.root.minsize(600, 600)
        
        self.scrollbar = tk.Scrollbar(self.root)
        self.scrollbar.pack( side = tk.RIGHT, fill = tk.BOTH )
        
        self.mylist = tk.Listbox(self.root, yscrollcommand = self.scrollbar.set )
        
        self.mylist.pack( side = tk.LEFT, fill = tk.BOTH, expand=True)
        self.scrollbar.config( command = self.mylist.yview )

        self.running = True

        self.root.mainloop()

        
    def update(self):
        
        if(not self.running):
            return
        
        self.mylist.delete(0, tk.END)
        for log in self.logger.getLogs():
            self.mylist.insert(tk.END, str(log))
        #self.mylist.pack( side = tk.LEFT, fill = tk.BOTH )
        #self.scrollbar.config( command = self.mylist.yview )

            