'''
Created on 12.04.2020

@author: chris
'''

from enum import Enum
from datetime import datetime
import json

class LOG_TYPE(Enum):
    DEBUG = ("debug")
    WARNING = ("warning"),
    ERROR = ("error"),
    INFO = ("info")
    
    def __init__(self, representation):
        self.represenation = representation


class Logger(object):
    '''
    classdocs
    '''   

    __instance = None
   
    @staticmethod 
    def getInstance():
        """ Static access method. """
        if Logger.__instance == None:
            Logger()
        return Logger.__instance
    
    def __init__(self):
        """ Virtually private constructor. """
        if Logger.__instance != None:
            raise Exception("This class is a singleton!")
        else:
            Logger.__instance = self
            self.logs = []
    
    # ============================================================================================ #  
    def addLogMessage(self, msg):
        if(len(self.logs)>200):
            self.logs.pop(0)
        self.logs.append(msg)
        
    # ============================================================================================ #  
    def clearLogMessage(self): 
        self.logs.clear()
    
    # ============================================================================================ # 
    def getLogs(self):
        return self.logs
        
    # ============================================================================================ #   
    def toJSON(self):
        return json.dumps(self.logs, default=lambda o: o.__dict__, indent=4)
        
        
class LoggerMessage(object):
    def __init__(self, logtype, message):
        self.message = message
        self.logtype = logtype.value
        now = datetime.now()
        self.timestamp = now.strftime("%H:%M:%S.%f")
        
    # ============================================================================================ #    
    def toJSON(self):
        return json.dumps(self, default=lambda o: o.__dict__, indent=4)
    
    # ============================================================================================ #  
    def __str__(self):
        return "[" + str(self.logtype) + "]: " + str(self.message) + " @" + str(self.timestamp)