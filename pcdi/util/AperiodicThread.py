'''
Created on 11.04.2020

@author: chris
'''

import threading

class AperiodicThread(threading.Thread):
    def __init__(self, execute, *args, **kwargs):
        threading.Thread.__init__(self)
        self.daemon = False
        self.stopped = False
        self.execute = execute
        self.args = args
        self.kwargs = kwargs

    def run(self):
        self.execute(*self.args, **self.kwargs)
