'''
Created on 11.04.2020

@author: chris
'''

import threading
import time

class StopableThread(threading.Thread):
    def __init__(self, delay, execute, *args, **kwargs):
        threading.Thread.__init__(self)
        self.daemon = False
        self.stopped = False
        self.delay = delay
        self.execute = execute
        self.args = args
        self.kwargs = kwargs
        
    def stop(self):
            self.stopped = True
            #self.join()
    def run(self):
        while not self.stopped:
            self.execute(*self.args, **self.kwargs)
            time.sleep(self.delay)