'''
Created on 23.01.2022

@author: langwch
'''

from pcdi.I_PCDIListener import I_PCDIListener
from pcdi.PCDIActions import PCDI_ACTIONS

from bootloader.bootloader import BOOTLOADER_COMMAND

import tkinter as tk
from tkinter.ttk import Progressbar
from tkinter import filedialog as fd
import threading
import time
import datetime
from util.StopableThread import StopableThread

from pcdi_over_ble.ble_adapter import BLE_Scanner, PCDI_BLE_Adapter


class PCDIGui(threading.Thread, I_PCDIListener):
    '''
    classdocs
    '''
    
    def __init__(self, presentation_layer, bootloader):
        
        self.presentation_layer = presentation_layer
        self.presentation_layer.registerListener(self)
        
        self.ble_scanner = BLE_Scanner()
        self.ble_scanner.addListener(self)
        
        self.bootloader = bootloader
        
        self.txt_conn = "connected: "
        
        self.WINDOW_WIDTH = 800
        self.WINDOW_HEIGTH = 800
        
        
        self.main_version = None
        self.sub_version = None
        self.build_number = None
        
        self.build_year = None
        self.build_month = None
        self.build_day = None
        self.build_hour = None
        self.build_minute = None
        
        self.is_bootloader = True
        
        
        self.updateThread = None
        self.updateInterval = 0.25
        
        self.slowThread = None
        self.slowInterval = 2
        
        self.action_done = False
        
        self.running = False
        
        threading.Thread.__init__(self)
        self.start()

    # ============================================================================================ #
    def callback(self):
        self.root.quit()

    # ============================================================================================ #
    def run(self):
        self.root = tk.Tk()
        self.root.minsize(self.WINDOW_WIDTH, self.WINDOW_HEIGTH)
        self.root.title("Odin - UCD Developemnt GUI")
        img = tk.PhotoImage(file='./application/logo.png')
        self.root.iconphoto(False, img)
        
        self.root.configure(background='white')
                
        self.discoverButton = tk.Button(self.root, text="discover", command = self.btnDiscoverCallback)
        self.discoverButton.place(x = 320, y = 10, width=100, height=60)
        
        self.bleDevicesList = tk.Listbox(self.root)
        self.bleDevicesList.place(x = 320, y = 80, width=150, height=200)
        
        self.connButton = tk.Button(self.root, text="connect", command = self.btnConnectCallback)
        self.connButton.place(x = 10, y = 10, width=100, height=60)
        
        self.bootloaderButton = tk.Button(self.root, text="bootloader", command = self.btnBootloaderCallback)
        self.bootloaderButton.place(x = 200, y = 10, width=100, height=30)
        
        self.rebootButton = tk.Button(self.root, text="reboot", command = self.btnRebootCallback)
        self.rebootButton.place(x = 200, y = 50, width=100, height=30)
        
        self.updateButton = tk.Button(self.root, text="update", command = self.btnUpdateCallback)
        self.updateButton.place(x = 10, y = 110, width=100, height=60)
        
        self.startAppButton = tk.Button(self.root, text="start application", command = self.startAppButtonCallback)
        self.startAppButton.place(x = 200, y = 110, width=100, height=60)
        
        self.lblConnectionState = tk.Label(self.root, text=self.txt_conn, bg="white", fg="red")
        self.lblConnectionState.place(x = 10, y = 210, width=300, height=60)
        
        self.lblSwVersion = tk.Label(self.root, text="", bg="white", fg="black")
        self.lblSwVersion.place(x = 10, y = 310, width=300, height=30)
        
        self.lblSwVersionApp = tk.Label(self.root, text="", bg="white", fg="black")
        self.lblSwVersionApp.place(x = 10, y = 340, width=300, height=30)
        
        self.progress = Progressbar(self.root, orient = tk.HORIZONTAL, length = 100, mode = 'determinate')
        self.progress.place(x = 320, y = 335, width=400, height=30)
        
        self.lblProgress = tk.Label(self.root, text="", bg="white", fg="black")
        self.lblProgress.place(x = 320, y = 310, width=400, height=20)
        
        self.scrollbarHistory = tk.Scrollbar(self.root)
        self.scrollbarHistory.place(x = 680, y = 450, width=10, height=150)
        self.history = tk.Listbox(self.root, yscrollcommand = self.scrollbarHistory.set )
        self.history.place(x = 0, y = 450, width=680, height=150)
        self.scrollbarHistory.config(command = self.history.yview)
                
        self.lblUzk = tk.Label(self.root, text="DC-Link: ", bg="white", fg="black", anchor="w", justify=tk.LEFT)
        self.lblUzk.place(x = 500, y = 10, width=200, height=20) 
        
        self.lbl24V = tk.Label(self.root, text="24V-Rail: ", bg="white", fg="black", anchor="w", justify=tk.LEFT)
        self.lbl24V.place(x = 500, y = 40, width=200, height=20) 
        
        self.lblSpeed = tk.Label(self.root, text="Door-Speed: ", bg="white", fg="black", anchor="w", justify=tk.LEFT)
        self.lblSpeed.place(x = 500, y = 70, width=200, height=20) 
        
        self.lblPosition = tk.Label(self.root, text="Door-Position: ", bg="white", fg="black", anchor="w", justify=tk.LEFT)
        self.lblPosition.place(x = 500, y = 100, width=200, height=20) 
        
        self.I1 = tk.IntVar()
        self.ck_I1 = tk.Checkbutton(self.root, text="I1", variable=self.I1, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_I1.place(x=500, y=130, width=100, height=20)
        
        self.I2 = tk.IntVar()
        self.ck_I2 = tk.Checkbutton(self.root, text="I2", variable=self.I2, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_I2.place(x=500, y=160, width=100, height=20)
        
        self.I3 = tk.IntVar()
        self.ck_I3 = tk.Checkbutton(self.root, text="I3", variable=self.I3, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_I3.place(x=500, y=190, width=100, height=20)
        
        self.I4 = tk.IntVar()
        self.ck_I4 = tk.Checkbutton(self.root, text="I4", variable=self.I4, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_I4.place(x=500, y=220, width=100, height=20)
        
        self.O1 = tk.IntVar()
        self.ck_O1 = tk.Checkbutton(self.root, text="O1", variable=self.O1, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_O1.place(x=600, y=130, width=100, height=20)
        
        self.O2 = tk.IntVar()
        self.ck_O2 = tk.Checkbutton(self.root, text="O2", variable=self.O2, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_O2.place(x=600, y=160, width=100, height=20)
        
        self.O3 = tk.IntVar()
        self.ck_O3 = tk.Checkbutton(self.root, text="O3", variable=self.O3, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_O3.place(x=600, y=190, width=100, height=20)
        
        self.O4 = tk.IntVar()
        self.ck_O4 = tk.Checkbutton(self.root, text="O4", variable=self.O4, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_O4.place(x=600, y=220, width=100, height=20)
        
        self.IPD = tk.IntVar()
        self.ck_IPD = tk.Checkbutton(self.root, text="IPD", variable=self.IPD, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_IPD.place(x=600, y=250, width=100, height=20)
        
        self.REF_SW = tk.IntVar()
        self.ck_REF_SW = tk.Checkbutton(self.root, text="REF", variable=self.REF_SW, state=tk.DISABLED, anchor="w", justify=tk.LEFT)
        self.ck_REF_SW.place(x=600, y=280, width=100, height=20)
        
        self.auto_driver = tk.IntVar()
        self.m_auto_driver = 0
        self.ck_auto_driver = tk.Checkbutton(self.root, text="Autodrive", variable=self.auto_driver, state=tk.NORMAL, anchor="w", justify=tk.LEFT)
        self.ck_auto_driver.place(x=500, y=280, width=100, height=20)
             
        self.v_com = tk.IntVar()
        self.ck_v_com = tk.Checkbutton(self.root, text="VirtualCom", variable=self.v_com, state=tk.NORMAL, anchor="w", justify=tk.LEFT)
        self.ck_v_com.place(x=10, y=180, width=100, height=20)
        
        self.scrollbarEventHistory = tk.Scrollbar(self.root)
        self.scrollbarEventHistory.place(x = 680, y = 650, width = 10, height=150)
        self.events = tk.Listbox(self.root, yscrollcommand = self.scrollbarEventHistory.set )
        self.events.place(x = 0, y = 650, width = 680, height = 150)
        self.scrollbarEventHistory.config(command = self.events.yview)
                

        self.lblkp = tk.Label(self.root, text="kP ", bg="white", fg="black", anchor="w", justify=tk.CENTER)
        self.lblkp.place(x = 350, y = 380, width=50, height=30) 
        self.kpTxt = tk.Entry(self.root, bd = 5)
        self.kpTxt.place(x=400, y=380, width=100, height=30)
        self.kpButton = tk.Button(self.root, text="write", command = self.btnKpCallback)
        self.kpButton.place(x = 500, y = 380, width=80, height=30)
            
        self.lblki = tk.Label(self.root, text="kI ", bg="white", fg="black", anchor="w", justify=tk.CENTER)
        self.lblki.place(x = 350, y = 420, width=50, height=30) 
        self.kiTxt = tk.Entry(self.root, bd = 5)
        self.kiTxt.place(x=400, y=420, width=100, height=30)
        self.kiButton = tk.Button(self.root, text="write", command = self.btnKiCallback)
        self.kiButton.place(x = 500, y = 420, width=80, height=30)
                
        self.updateThread = StopableThread(delay=self.updateInterval, execute=self.cyclic)
        self.updateThread.start()
        
        self.slowThread = StopableThread(delay=self.slowInterval, execute=self.slowIntervalCylic)
        self.slowThread.start()
        
        self.updateButton["state"] = "disabled"
        self.startAppButton["state"] = "disabled"
        self.rebootButton["state"] = "disabled"
        self.bootloaderButton["state"] = "disabled"
  
        self.running = True
        self.root.mainloop()
    
    # ============================================================================================ #  
    def cyclic(self):
        if(not self.running):
            return
        
        res = self.bootloader.getProgress()
        self.lblProgress['text'] = str(res['actual_line']) + " of " + str(res['lines'])
        self.progress['value'] = res['progress']*100
        
        
    
    # ============================================================================================ # 
    def slowIntervalCylic(self):
        if(not self.running):
            return 
        
        if(self.is_bootloader == False and self.presentation_layer.isConnected()):

            if(self.build_day is None):
                self.presentation_layer.readParameter(0, 2)
            if(self.build_month is None):
                self.presentation_layer.readParameter(0, 3)
            if(self.build_year is None):
                self.presentation_layer.readParameter(0, 4)
            if(self.main_version is None):
                self.presentation_layer.readParameter(0, 7)
            if(self.sub_version is None):
                self.presentation_layer.readParameter(0, 8)
            if(self.build_number is None):
                self.presentation_layer.readParameter(0, 9)
                
            self.presentation_layer.readParameter(4, 23)
            self.presentation_layer.readParameter(4, 24)
            self.presentation_layer.readParameter(1, 27)
            self.presentation_layer.readParameter(1, 29)
            self.presentation_layer.readParameter(1, 30)
            self.presentation_layer.readParameter(4, 22)
            
            self.presentation_layer.readParameter(2, 35)
            
            self.presentation_layer.readParameter(3, 25)
            self.presentation_layer.readParameter(3, 26)
            #self.presentation_layer.readParameter(1, 20)
             
            if(self.auto_driver.get() != self.m_auto_driver):
                self.presentation_layer.writeParameter(4, 22, self.auto_driver.get())
                self.m_auto_driver = self.auto_driver.get()
            
    # ============================================================================================ #  
    def update(self):
        
        pass
        
        #self.lbldebug['text'] = 
    
    # ============================================================================================ #
    def btnKpCallback(self):
        val = float(self.kpTxt.get())
        self.presentation_layer.writeParameter(3, 25, val)
       
    # ============================================================================================ #
    def btnKiCallback(self):
        val = float(self.kiTxt.get())
        self.presentation_layer.writeParameter(3, 26, val)
         
    # ============================================================================================ #
    def btnConnectCallback(self):
        
        if(self.v_com.get()):
            
            if(self.presentation_layer.isConnected()):
                self.presentation_layer.disconnect()
            else:
                self.presentation_layer.connect()
            return
        
        
        if(self.presentation_layer.isConnected()):
            self.presentation_layer.disconnect()
        else:
            sel = self.bleDevicesList.curselection()
            if(len(sel) > 0):
                self.ble_scanner.stop()
                tmp = self.bleDevicesList.get(sel[0])
                tmp1 = tmp.split("{")
                ble_address = tmp1[1].replace("}", "")
                self.presentation_layer.transportation_layer.physical_layer = PCDI_BLE_Adapter(ble_address)
                self.presentation_layer.connect()
        
         
    # ============================================================================================ #
    def btnBootloaderCallback(self):
        if(self.presentation_layer.isConnected()):
            self.action_done = False
            self.presentation_layer.triggerAction(PCDI_ACTIONS.JUMP_TO_BOOTLOADER)
            while self.action_done == False:
                time.sleep(0.1)
            self.presentation_layer.disconnect()
            
            time.sleep(1)
            self.presentation_layer.connect()
    
    # ============================================================================================ #
    def btnRebootCallback(self):
        if(self.presentation_layer.isConnected()):   
            self.presentation_layer.triggerAction(PCDI_ACTIONS.REBOOT)
            
    # ============================================================================================ #
    def btnUpdateCallback(self):
        if(self.presentation_layer.isConnected()):
            
            filetypes = (('bootloader files', '*.bootloader'),('All files', '*.*'))

            filename = fd.askopenfilename(title='Open a bootloader-file',initialdir='/',filetypes=filetypes)
            
            #self.bootloader.doUpdate("C:/Users/langwch/git/nd_midisuprav2/midisupra_nd/Debug/midisupra_nd.bootloader")
            #self.bootloader.doUpdate("C:/Users/langwch/git/ucd/ucd_l431/Debug/ucd_l431.bootloader")
            #print(filename)
            self.bootloader.doUpdate(filename)
        else:
            tk.messagebox.showerror("Error - No Connection", "Not Connected to Drive") 

    # ============================================================================================ #    
    def startAppButtonCallback(self):
        if(self.presentation_layer.isConnected()):
            self.presentation_layer.sendBootloaderMessage(BOOTLOADER_COMMAND.START_APP)
        else:
            tk.messagebox.showerror("Error - No Connection", "Not Connected to Drive") 
            
    # ============================================================================================ #     
    def btnResetDriveCallback(self):
        self.presentation_layer.triggerAction(PCDI_ACTIONS.REBOOT)

    # ============================================================================================ #     
    def btnDiscoverCallback(self):
        if(self.ble_scanner.is_scanning):
            self.ble_scanner.stop()
            print("stop discovering")     
        else:
            self.ble_scanner.scan_for_devices()
            print("start discovering")      
 
    # ============================================================================================ #
    def notifyDeviceDiscovered(self, device):        
        repr_str = str(device.name) + " {" + str(device.address) + "}"
        self.bleDevicesList.insert(tk.END, repr_str)
        
    # ============================================================================================ #

    # I_PCDIListener Interface - Realization
    def notifyConnectionChanged(self, isConnected):
        # connection state
        self.lblConnectionState['text'] = self.txt_conn + str(isConnected) 
        if(isConnected):
            self.lblConnectionState['fg'] = "green"
            self.connButton['text'] = "disconnect" 
            
            self.updateButton["state"] = "normal"
            self.startAppButton["state"] = "normal"
            self.rebootButton["state"] = "normal"
            self.bootloaderButton["state"] = "normal"
                
            self.presentation_layer.sendBootloaderMessage(BOOTLOADER_COMMAND.GET_BL_VERSION)
            self.presentation_layer.sendBootloaderMessage(BOOTLOADER_COMMAND.GET_APP_VERSION)
        else:
            self.lblConnectionState['fg'] = "red"
            self.connButton['text'] = "connect" 
            self.lblSwVersion['text'] = ""
            self.lblSwVersionApp['text'] = ""
            self.updateButton["state"] = "disabled"
            self.startAppButton["state"] = "disabled"
            self.rebootButton["state"] = "disabled"
            self.bootloaderButton["state"] = "disabled"
            
            self.main_version = None
            self.sub_version = None
            self.build_number = None
            
            self.build_year = None
            self.build_month = None
            self.build_day = None
            self.build_hour = None
            self.build_minute = None
        

    def notifyParameterReceived(self, parameter):
        self.history.insert(tk.END, str(parameter))
        
        if(parameter.module_id == 4 and parameter.parameter_id == 24):
            self.lblSpeed["text"] = "Door-Speed: " + str(round(parameter.value, 3)) + "m/s"
        elif(parameter.module_id == 4 and parameter.parameter_id == 23):
            self.lblPosition["text"] = "Door-Position: " + str(round(parameter.value,3)) + "m"
        elif(parameter.module_id == 1 and parameter.parameter_id == 27):
            self.lblUzk["text"] = "DC-Link: " + str(round(parameter.value,3)) + "V"  
        elif(parameter.module_id == 1 and parameter.parameter_id == 29):
            self.lbl24V["text"] = "24V-Rail: " + str(round(parameter.value,3)) + "V"  
            
        elif(parameter.module_id == 1 and parameter.parameter_id == 30):
            
            inputs = parameter.value&0xFFFF
            if(inputs&0x1):
                self.I1.set(1)
            else:
                self.I1.set(0)
            
            if(inputs&0x2):
                self.I2.set(1)
            else:
                self.I2.set(0)
            
            if(inputs&0x4):
                self.I3.set(1)
            else:
                self.I3.set(0)
            
            if(inputs&0x8):
                self.I4.set(1)
            else:
                self.I4.set(0)
                
            if(inputs&1<<8):
                self.IPD.set(1)
            else:
                self.IPD.set(0)
            
            if(inputs&1<<9):
                self.REF_SW.set(1)
            else:
                self.REF_SW.set(0)
                
            outputs = (parameter.value >> 16)&0xFFFF
        
            if(outputs&1):
                self.O1.set(1)
            else:
                self.O1.set(0)
            
            if(outputs&1<<1):
                self.O2.set(1)
            else:
                self.O2.set(0)
                
            if(outputs&1<<2):
                self.O3.set(1)
            else:
                self.O3.set(0)
                
            if(outputs&1<<3):
                self.O4.set(1)
            else:
                self.O4.set(0)
                

            
        if(parameter.module_id == 0):
            if(parameter.parameter_id == 2): 
                self.build_day = parameter.value
            elif(parameter.parameter_id == 3):
                self.build_month = parameter.value
            elif(parameter.parameter_id == 4):
                self.build_year = parameter.value
            elif(parameter.parameter_id == 5):
                self.build_hour = parameter.value
            elif(parameter.parameter_id == 6):
                self.build_minute = parameter.value
            elif(parameter.parameter_id == 7):
                self.main_version = parameter.value
            elif(parameter.parameter_id == 8):
                self.sub_version = parameter.value
            elif(parameter.parameter_id == 9):
                self.build_number = parameter.value
            

            self.lblSwVersion['text'] = "UCD v " + str(self.main_version) + "." + str(self.sub_version) + " build" + str(self.build_number) + ", " + str(self.build_day) + "." + str(self.build_month) + "." + str(self.build_year)
            
    def notifyEvent(self, event):
        self.events.insert(tk.END, str(event))
    
    def notifyCommandDone(self):
        self.action_done = True
        pass
    
    def notifySoftScopeCommandDone(self, softscope_cmd, triggered = None, done = None, channel = None, index = None, result = None):
        pass
    
    def notifyBootloaderCommandDone(self, bootloader_cmd, payload = None):
        #print(bootloader_cmd)
        if(bootloader_cmd == BOOTLOADER_COMMAND.GET_BL_VERSION):
            #print(payload)
            self.is_bootloader = True
            if(len(payload) >= 7):
                main_version = str(payload[0])
                sub_version = str(payload[1])
                build_day = str(payload[2])
                build_month = str(payload[3])
                build_year = str(payload[4])
                build_hour = str(payload[5])
                build_minute = str(payload[6])
                
                self.lblSwVersion['text'] = 'Bootloader: v' + main_version + "." + sub_version + " @ " + build_day + "." + build_month + "." + build_year + ", " + build_hour + ":" + build_minute
        elif(bootloader_cmd == BOOTLOADER_COMMAND.GET_APP_VERSION):
            self.is_bootloader = True
            if(len(payload) >= 10):
                app_main_version = str(payload[0])
                app_minor_version = str(payload[1])
                app_build = str(payload[2]*256+payload[3])
                
                app_day = str(payload[4])
                app_month = str(payload[5])
                app_year = str(payload[6]*256+payload[7])
                
                app_hour = str(payload[8])
                app_minute = str(payload[9])
                self.lblSwVersionApp['text'] = "UCD v" + app_main_version + "." + app_minor_version + ":" + app_build + " @ " + app_day + "." + app_month + "." + app_year + ", " + app_hour + ":" + app_minute
        
        elif(bootloader_cmd == BOOTLOADER_COMMAND.INVALID):
            self.is_bootloader = False
            
        
        pass
        #print(bootloader_cmd)
        #print("test")
        
    def notifyDebugReadDone(self, address, byte_size, data):
        pass