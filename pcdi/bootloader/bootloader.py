'''
Created on 19.06.2023

@author: langwch
'''

from enum import Enum
import binascii
import time

from pcdi.I_PCDIListener import I_PCDIListener
from util.AperiodicThread import AperiodicThread

class BOOTLOADER_COMMAND(Enum):

    START_BL = 1,
    GET_BL_VERSION = 2,
    GET_APP_VERSION = 3,
    ERASE_FLASH = 5,
    SET_ADDRESS_PTR = 6,
    WRITE_DATA = 7,
    FLUSH = 8,
    FINISH_UPDATE = 9,
    START_APP = 10,
    ERASE_PAGE = 20,
    
    ACK = 200, 
    NACK = 201,
    BUSY = 202

    INVALID = (255)
    def __init__(self, type_id):
        self.type_id = type_id
        
    def getId(self):
        return self.type_id  
    
    @classmethod
    def fromInteger(cls, value):
        for e in BOOTLOADER_COMMAND:
            if(e.getId() == value):
                return e
        return BOOTLOADER_COMMAND.INVALID

class Bootloader(I_PCDIListener):
    '''
    classdocs
    '''


    def __init__(self, pcdi_presentation_layer):
        '''
        Constructor
        '''
        
        pcdi_presentation_layer.registerListener(self)
        self.pcdi = pcdi_presentation_layer
        self.isConnected = False
        
        self.ACK = False
        
        self.bootloader_file_path = ""
        
        self.aThread = None
        
        self.lines = 0
        self.line_cnt = 0
        self.updateActive = False
        
    def doUpdate(self, bootloader_file_path):
        
        if(self.aThread is None):
            self.bootloader_file_path = bootloader_file_path
            self.aThread = AperiodicThread(execute=self.worker)
            self.aThread.start()


    def worker(self):
        self.updateActive = True
        f = open(self.bootloader_file_path, "r")
        self.lines = sum(1 for _ in f)
        f.close
        
        f = open(self.bootloader_file_path, "r")
        
        self.line_cnt = 0
        
        for line in f:
            
            self.ACK = False
            
            splitted_line = line.split(':')
            
            line_bytes = int(splitted_line[1]).to_bytes(2, 'big')
            msg_payload = [line_bytes[0], line_bytes[1]]
            
            if(len(splitted_line) >= 3):
                
                #print(splitted_line[2])
                payload_as_bytes = bytearray.fromhex(splitted_line[2].strip())
                
                for b in payload_as_bytes:
                    msg_payload.append(b)
            
            #print(BOOTLOADER_COMMAND.fromInteger(int(splitted_line[0])))
            
            cmd = BOOTLOADER_COMMAND.fromInteger(int(splitted_line[0]))
            
            self.pcdi.sendBootloaderMessage(cmd, payload = msg_payload)
            
            while(self.ACK == False):
                time.sleep(0.001)
                
            self.line_cnt = self.line_cnt + 1
            
        f.close
        self.aThread = None
        self.updateActive = False

    def getProgress(self):
        if(self.lines > 0):
            return {'lines': self.lines, 'actual_line' : self.line_cnt, 'progress' : self.line_cnt / self.lines}
        else:
            return {'lines': self.lines, 'actual_line' : self.line_cnt, 'progress' : 0}
        
    def isUpdateRunning(self):
        return self.updateActive

    def notifyConnectionChanged(self, isConnected):
        self.isConnected = isConnected
    
    def notifyParameterReceived(self, parameter):
        pass

    def notifyEvent(self, event):
        pass
    
    def notifySoftScopeCommandDone(self, softscope_cmd, triggered = None, done = None, channel = None, index = None, result = None, module_id = None, parameter_id = None, gain = None):
        pass
    
    def notifyBootloaderCommandDone(self, bootloader_cmd, payload = None):
        if(bootloader_cmd == BOOTLOADER_COMMAND.ACK):
            print("got ACK from bootloader")
            self.ACK = True
        elif(bootloader_cmd == BOOTLOADER_COMMAND.NACK):
            print("!!!!!! got NACK from bootloader")
            
        #print(bootloader_cmd)
        pass
    
    def notifyCommandDone(self):
        pass
    
    def notifyDebugReadDone(self, address, byte_size, data):
        pass