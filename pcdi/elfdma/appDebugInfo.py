'''
Created on 27.07.2023

@author: langwch
'''

import struct
import json

class DebugVariable(object):
    
    def __init__(self, name, address, byte_size, data_type):
        self.name = name
        self.address = address
        self.byte_size = byte_size
        self.data_type = data_type
        
        self.raw_data = None
        
    def setRawData(self, raw_data):
        self.raw_data = raw_data
        
    def getValue(self):
        
        if(self.raw_data is None):
            return 0
        
        if(('signed char' == self.data_type) or ('char' == self.data_type)):
            if(len(self.raw_data) != 1):
                raise ValueError('Raw data size does not match assigned data-type')
            
            res = struct.unpack('b', bytearray(self.raw_data))
            return res[0]
        
        elif('unsigned char' == self.data_type):
            if(len(self.raw_data) != 1):
                raise ValueError('Raw data size does not match assigned data-type')
            res = struct.unpack('B', bytearray(self.raw_data))
            return res[0]
        
        elif('_Bool' == self.data_type):
            if(len(self.raw_data) != 1):
                raise ValueError('Raw data size does not match assigned data-type')
            res = struct.unpack('?', bytearray(self.raw_data))
            return res[0]
        
        elif('short int' == self.data_type):
            if(len(self.raw_data) != 2):
                raise ValueError('Raw data size does not match assigned data-type')
            res = struct.unpack('<h', bytearray(self.raw_data))
            return res[0]
        
        elif('short unsigned int' == self.data_type):
            if(len(self.raw_data) != 2):
                raise ValueError('Raw data size does not match assigned data-type')
            res = struct.unpack('<H', bytearray(self.raw_data))
            return res[0]
        
        elif(('long int' == self.data_type) or ('int' == self.data_type)):
            if(len(self.raw_data) != 4):
                raise ValueError('Raw data size does not match assigned data-type')
            res = struct.unpack('<i', bytearray(self.raw_data))
            return res[0]
        
        elif(('long unsigned int' == self.data_type) or ('unsigned int' == self.data_type)):
            if(len(self.raw_data) != 4):
                raise ValueError('Raw data size does not match assigned data-type')
            res = struct.unpack('<I', bytearray(self.raw_data))
            return res[0]
        
        elif('float' == self.data_type):
            if(len(self.raw_data) != 4):
                raise ValueError('Raw data size does not match assigned data-type')
            res = struct.unpack('<f', bytearray(self.raw_data))
            return res[0]
        
    def toDict(self):
        return {'name' : str(self.name), 
                'value' : self.getValue(), 
                'address' : hex(self.address), 
                'size': self.byte_size, 
                'type' : str(self.data_type)}
        
    def __str__(self):
        return str(self.name) + ": " + str(self.getValue()) + " || address: " + hex(self.address) + ", type: " + str(self.data_type) + "||"
        
class AppDebugInfo(object):

    def __init__(self, json_filepath):

        f = open(json_filepath)
        self.data = json.load(f)
        f.close()
        
        self.debug_variables =  []
        
        for l in self.data['vars']:
            self.debug_variables.append(DebugVariable(l['name'], l['address'], l['size'], l['type']))
    
    def getDebugVariables(self):
        return self.debug_variables  
            
    def getVariableByAddress(self, address):
        for v in self.debug_variables:
            if(v.address == address):
                return v  
        print("did not find: " + str(address))
        return None
    
    def getVariableByName(self, name):
        for v in self.debug_variables:
            if(v.name == name):
                return v
            
        return None
        