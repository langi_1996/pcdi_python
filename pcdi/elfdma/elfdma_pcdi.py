'''
Created on 27.07.2023

@author: langwch
'''

from pcdi.I_PCDIListener import I_PCDIListener

class ElfDmaPcdi(I_PCDIListener):

    def __init__(self, pcdi, app_debug_info):
        
        self.pcdi = pcdi 
        self.app_debug_info = app_debug_info
        
        self.pcdi.registerListener(self)
    
    def readVariable(self, name):
        v = self.app_debug_info.getVariableByName(name)
        if(v is not None):
            self.pcdi.debugRead(v.address, v.byte_size)
    
    def notifyConnectionChanged(self, isConnected):
        if(isConnected):
            self.readVariable('door_info.door_width')
            self.readVariable('jlt_coupler_close.st.v2')
            self.readVariable('currentController.in.enable_decoupling')
        pass
    
    def notifyParameterReceived(self, parameter):
        pass

    def notifyEvent(self, event):
        pass
    
    def notifyCommandDone(self):
        pass
    
    def notifySoftScopeCommandDone(self, softscope_cmd, triggered = None, done = None, channel = None, index = None, result = None, module_id = None, parameter_id = None, gain = None):
        pass
    
    def notifyBootloaderCommandDone(self, bootloader_cmd, payload = None):
        pass
    
    def notifyDebugReadDone(self, address, byte_size, data):
        
        v = self.app_debug_info.getVariableByAddress(address)
        if(v is not None):
            v.setRawData(data)
            print(v)
        