'''
Created on 15.01.2022

@author: langwch
'''

import sys
import csv

from pcdi.VirtualComPort import VirtualComPort
from pcdi.PCDIMaster import PCDIMaster
from pcdi.PCDIParser import PCDIParser 

from pcdi.PCDIMessage import PCDIMessage, PCDI_MESSAGE_ID
from pcdi.PCDISoftScopeCommands import PCDI_SOFTSCOPE_COMMAND

from pcdi.I_PCDIListener import I_PCDIListener

from util.StopableThread import StopableThread

from application.GUI import PCDIGui

import matplotlib.pyplot as plt
import numpy as np
import time

from bootloader.bootloader import Bootloader

from pcdi_over_telnet.telnet_adapter import PCDI_Telnet_Adapter
from pcdi_over_telnet.tcp_adapter import PCDI_TCP_Adapter

from elfdma.appDebugInfo import AppDebugInfo, DebugVariable
from elfdma.elfdma_pcdi import ElfDmaPcdi

from pcdi_over_ble.ble_adapter import PCDI_BLE_Adapter

class PCDIPrinter(I_PCDIListener):
    def __init__(self, presentation_layer):
        self.presentation_layer = presentation_layer
        presentation_layer.registerListener(self)
        
        self.numberOfModules = 0
        self.cyclicThread = None
        self.stateCnt = 0
        self.scopeDone = False
        
        self.scopeBuf_ch0 = []
        self.scopeBuf_ch1 = []
        self.scopeBuf_ch2 = []
        self.scopeBuf_ch3 = []
        
        self.ch0_rqu = False
        self.ch1_rqu = False
        self.ch2_rqu = False
        self.ch3_rqu = False
        
    
    def notifyConnectionChanged(self, isConnected):
        if isConnected:
            self.presentation_layer.readParameter(0,1)
            
            
            self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.CONFIG, undersample = 100, trigger_level = 0.5)
            self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL, channel = 0, module_id = 3, param_id = 5, gain = 1.0)
            self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL, channel = 1, module_id = 1, param_id = 21, gain = 1.0)
            self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL, channel = 2, module_id = 1, param_id = 22, gain = 1.0)
            self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL, channel = 3, module_id = 1, param_id = 26, gain = 1.0)
            self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.RESET)
            
            
            if(self.cyclicThread is None):
                self.cyclicThread = StopableThread(delay=0.02, execute=self.cyclic)
                self.cyclicThread.start()
        print("Connection: " + str(isConnected))
    
    def notifyParameterReceived(self, parameter):
        if(parameter.parameter_id == 1 and parameter.module_id == 0 and self.numberOfModules == 0):
            self.numberOfModules = parameter.value
            
            for i in range(0, self.numberOfModules):
                self.presentation_layer.readParameter(i,0)
        if(parameter.parameter_id == 0):
            for j in range(1, parameter.value):
                self.presentation_layer.readParameter(parameter.module_id,j)
            

    def notifyEvent(self, event):
        print(event)

    def notifySoftScopeCommandDone(self, softscope_cmd, triggered = None, done = None, channel = None, index = None, result = None):
        
        if(softscope_cmd == PCDI_SOFTSCOPE_COMMAND.GET_STATE):
            #print("triggered: " + str(triggered) + ", done: " + str(done))
            if(done):
                self.scopeDone = True
            else:
                self.scopeDone = False
         
        elif(softscope_cmd == PCDI_SOFTSCOPE_COMMAND.CONFIG):
            print("SoftScope Configuration written successfully")
            
        elif(softscope_cmd == PCDI_SOFTSCOPE_COMMAND.CONFIG_CHANNEL):
            print("SoftScope Channel Configuration written successfully")
            
        elif(softscope_cmd == PCDI_SOFTSCOPE_COMMAND.RESET):
            print("SoftScope reset done")
            
        elif(softscope_cmd == PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL):
            if(channel == 0):
                self.scopeBuf_ch0.append(result)
                self.ch0_rqu = False
            elif(channel == 1):
                self.scopeBuf_ch1.append(result)
                self.ch1_rqu = False
            elif(channel == 2):
                self.scopeBuf_ch2.append(result)
                self.ch2_rqu = False
            elif(channel == 3):
                self.scopeBuf_ch3.append(result)
                self.ch3_rqu = False
        #print(softscope_cmd)

    def notifyCommandDone(self):
        pass

    def notifyBootloaderCommandDone(self, bootloader_cmd, payload = None):
        pass
        #print(bootloader_cmd)

    def notifyDebugReadDone(self, address, byte_size, data):
        pass

    def cyclic(self):
        
        #self.presentation_layer.readParameter(1,20)
        
        
        if(self.stateCnt > 500):
            self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.GET_STATE)
            self.stateCnt = 0
            
            #print(len(self.scopeBuf_ch0))
            #print(len(self.scopeBuf_ch1))
            #print(len(self.scopeBuf_ch2))
            #print(len(self.scopeBuf_ch3))
            
        if(self.scopeDone):
            if(len(self.scopeBuf_ch0)+1 < 256 and self.ch0_rqu == False):
                self.ch0_rqu = True
                self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL, channel = 0, index = len(self.scopeBuf_ch0)+1)
            if(len(self.scopeBuf_ch1)+1 < 256 and self.ch1_rqu == False):
                self.ch1_rqu = True
                self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL, channel = 1, index = len(self.scopeBuf_ch1)+1)
            if(len(self.scopeBuf_ch2)+1 < 256 and self.ch2_rqu == False):
                self.ch2_rqu = True
                self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL, channel = 2, index = len(self.scopeBuf_ch2)+1)
            if(len(self.scopeBuf_ch3)+1 < 256 and self.ch3_rqu == False):
                self.ch3_rqu = True
                self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.READ_CHANNEL, channel = 3, index = len(self.scopeBuf_ch3)+1)
        
        
            if(len(self.scopeBuf_ch0) >= 255 and len(self.scopeBuf_ch1) >= 255 and len(self.scopeBuf_ch2) >= 255 and len(self.scopeBuf_ch3) >= 255):
                self.scopeDone = False
                #pcdi_presentation_layer.writeParameter(3, 5, 0.0)
                self.presentation_layer.sendScopeCommand(PCDI_SOFTSCOPE_COMMAND.RESET)
                print("scope done")
                plt.plot(self.scopeBuf_ch0, label='ch0')
                plt.plot(self.scopeBuf_ch1, label='ch1')
                plt.plot(self.scopeBuf_ch2, label='ch2')
                plt.plot(self.scopeBuf_ch3, label='ch3')
                plt.grid()
                plt.legend(loc="upper left")
                plt.show()
                
                with open('scope.csv', 'w', encoding='UTF8') as f:
                    writer = csv.writer(f)
                
                    for i in range(0, len(self.scopeBuf_ch0)):
                        
                        # write the data
                        writer.writerow([self.scopeBuf_ch0[i], self.scopeBuf_ch1[i], self.scopeBuf_ch2[i],self.scopeBuf_ch3[i]])
                
                self.scopeBuf_ch0 = []
                self.scopeBuf_ch1 = []
                self.scopeBuf_ch2 = []
                self.scopeBuf_ch3 = []
            
        self.stateCnt = self.stateCnt + 1
    
        
if __name__ == '__main__':
    print(sys.version)
    
    app_debug_info = AppDebugInfo('./variables.json')
    
    
    #raise KeyboardInterrupt('') 
    
    use_telnet = False
    
    if(use_telnet):
        #pcdi_physical_layer = PCDI_Telnet_Adapter("192.168.4.1", 23)
        pcdi_physical_layer = PCDI_TCP_Adapter("192.168.4.1", 23)
    else:
        pcdi_physical_layer = VirtualComPort("COM5")
        #pcdi_physical_layer = PCDI_BLE_Adapter("48:23:35:37:03:38")
        
    pcdi_transportation_layer = PCDIMaster(pcdi_physical_layer)
    pcdi_presentation_layer = PCDIParser(pcdi_transportation_layer)
        
        
    #elf_dma_pcdi = ElfDmaPcdi(pcdi_presentation_layer, app_debug_info)
        
    #printer = PCDIPrinter(pcdi_presentation_layer)
    
    bootloader = Bootloader(pcdi_presentation_layer)
    
    #pcdi_presentation_layer.connect()
    
    myGUI = PCDIGui(pcdi_presentation_layer, bootloader)
    
    #pcdi_presentation_layer.readParameter(0, 0)
    while True:
        try:
            #res = input("type in value for V3.5")
            #if(pcdi_presentation_layer.isConnected()):
            #    pcdi_presentation_layer.writeParameter(3, 5, float(res))
            time.sleep(1)
            
        except KeyboardInterrupt as ex:
            sys.exit(0)
        
        