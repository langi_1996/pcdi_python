'''
Created on 21.06.2023

@author: langwch
'''

import telnetlib

from pcdi.I_PhysicalLayer import I_PhysicalLayer
import copy

class PCDI_Telnet_Adapter(I_PhysicalLayer):
    '''
    classdocs
    '''


    def __init__(self, ip, port):
        '''
        Constructor
        '''
        self.ip = ip        
        self.port = port 
        self.is_open = False
        self.rx_buffer = []
        
        
    def open(self):
        self.tn = telnetlib.Telnet(self.ip, port = self.port)
        self.is_open = True
    
    def close(self):
        self.tn.close()
        self.is_open = False
    
    def isOpen(self):
        return self.is_open

    def switchBaudrate(self, baudrate):
        pass
    
    def configureForPCDIMode(self):
        pass
    
    def configureForBootloaderMode(self):
        pass
    
    def resetCommunicationBuffer(self):
        self.rx_buffer = []
    
    def writeAll(self, data):
        print(type(data))
        self.tn.write(data)
    
    def readAll(self):
        
        tmp = self.tn.read_eager()
        for x in tmp:
            self.rx_buffer.append(x)
        
        res = copy.deepcopy(self.rx_buffer)
        self.rx_buffer = []
        return res

    def numberOfBytesInWaiting(self):
        res = self.tn.read_eager()
        for x in res:
            self.rx_buffer.append(x)
        return len(self.rx_buffer)

if __name__ == '__main__':
    
    
    test = PCDI_Telnet_Adapter("192.168.4.1", 23)
    
    