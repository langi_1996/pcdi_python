'''
Created on 21.06.2023

@author: langwch
'''

import telnetlib

from pcdi.I_PhysicalLayer import I_PhysicalLayer
import copy

from util.StopableThread import StopableThread

class PCDI_TCP_Adapter(I_PhysicalLayer):
    '''
    classdocs
    '''


    def __init__(self, ip, port):
        '''
        Constructor
        '''
        self.ip = ip        
        self.port = port 
        self.is_open = False
        self.rx_buffer = []
        self.cyclicThread = None
        self.cyclicInterval_s = 0
        
    def open(self):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((self.ip, self.port))
        
        if(self.cyclicThread is None):
            self.cyclicThread = StopableThread(delay=self.cyclicInterval_s, execute=self.cyclic)
            self.cyclicThread.start()
        
        self.is_open = True
    
    def close(self):
        if(self.cyclicThread is not None):
            self.cyclicThread.stop()
            self.cyclicThread = None
        self.s.close()
        self.is_open = False
    
    def isOpen(self):
        return self.is_open

    def switchBaudrate(self, baudrate):
        pass
    
    def configureForPCDIMode(self):
        pass
    
    def configureForBootloaderMode(self):
        pass
    
    def resetCommunicationBuffer(self):
        self.rx_buffer = []
    
    def writeAll(self, data):
        #print(type(data))
        self.s.sendall(data)
    
    def cyclic(self):
        try:
            b =  int.from_bytes(self.s.recv(1), "big")
            self.rx_buffer.append(b)
        except ConnectionAbortedError:
            pass
    
    def readAll(self):     
        #print(self.rx_buffer)   
        res = copy.deepcopy(self.rx_buffer)
        self.rx_buffer = []
        return res

    def numberOfBytesInWaiting(self):
        #print(self.rx_buffer)
        return len(self.rx_buffer)


    
    